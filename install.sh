mkdir -p /usr/local/share/bettercap/caplets/
cp wifi_seka_jsinject.js /usr/local/share/bettercap/caplets/
cp -r wifi_seka /opt/

cd /opt
apt-get update && apt-get install python3 python3-pip python3-venv  python3-dev
python3 -m venv wifi-seka-venv
source /opt/wifi-seka-venv/bin/activate
pip3 install django==1.11
pip3 install requests
cd wifi_seka/
python3 manage.py migrate
apt install gettext
django-admin compilemessages
echo "Create admin user for wifi-seka"
python3 manage.py createsuperuser
deactivate

#install ap tools
cd /opt
git clone https://github.com/oblique/create_ap.git
cd create_ap
make install

cd /opt
apt-get install mana-toolkit hostapd
git clone https://github.com/sensepost/berate_ap.git
cd berate_ap
make install

#install bettercap
apt-get install bettercap

cat << EOF > /etc/systemd/system/wifi_seka.service
[Unit]
Description=Wifi-Seka service
[Service]
ExecStart=source /opt/wifi-seka-venv/bin/activate && python3 /opt/wifi_seka/manage.py runserver 0.0.0.0:80
Restart=always
KillSignal=SIGQUIT
Type=notify
NotifyAccess=all
[Install]
WantedBy=multi-user.target

EOF

source /opt/wifi-seka-venv/bin/activate
nohup python3 /opt/wifi_seka/manage.py runserver 0.0.0.0:80 &
