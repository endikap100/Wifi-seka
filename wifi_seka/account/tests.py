from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.
class account_login_Tests(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()


    def test_logged_in_correct_password(self):
        response = self.client.login(username='admin', password='adminadmin')
        self.assertTrue(response)

    def test_logged_in_incorrect_password(self):
        response = self.client.login(username='admin', password='adminadminadmin')
        self.assertFalse(response)
