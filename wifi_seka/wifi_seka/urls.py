"""wifi_seka URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, i18n
from django.contrib import admin

app_name = 'Wifi-seka'
urlpatterns = [
    #url(r"^$",include("wseka_data.urls")),
    url(r"^",include("wseka_data.urls")),
    url(r'^admin/', admin.site.urls),
    url(r'^account/',include("account.urls")),
    url(r'^settings/',include("wseka_settings.urls")),
    #url(r'^data/',include("wseka_data.urls")),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]
