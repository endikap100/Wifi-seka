from django.apps import AppConfig


class WsekaDataConfig(AppConfig):
    name = 'wseka_data'
