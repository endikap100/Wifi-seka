from django.db import models

# Create your models here.
class Requests(models.Model):
    domain_name = models.CharField(max_length=200)
    requested_times = models.IntegerField(default=0)
    protocol = models.CharField(max_length=200)

    def __str__(self):
        return self.domain_name

    def increase(self):
        self.requested_times = models.F('requested_times') + 1
        self.save()
