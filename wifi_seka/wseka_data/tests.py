from django.test import TestCase
from django.urls import reverse
from .models import Requests

# Create your tests here.
class Data_export_Tests(TestCase):

    def test_export_data_with_objects(self):
        Requests.objects.create(domain_name="testobject",requested_times="2", protocol="http")
        response = self.client.get(reverse('export_data'))
        self.assertContains(response,"testobject")

    def test_export_data_without_objects(self):
        response = self.client.get(reverse('export_data'))

        self.assertEqual(response.content, b'[]')
        self.assertEqual(response.status_code, 200)
