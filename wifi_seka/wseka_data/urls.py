from django.conf.urls import url
from wseka_data.views import DataPageView, collect_data, export_data

urlpatterns = [
    url(r"^$", DataPageView.as_view(), name='home'),
    url(r'^collect_data/$', view=collect_data, name='collect_data'),
    url(r'^export_data/$', view=export_data, name='export_data')
]