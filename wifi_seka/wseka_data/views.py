from django.shortcuts import get_object_or_404,render, get_list_or_404
from django.views.generic.base import TemplateView
from django.http import HttpResponse
from WifiDeployAttack.WifiDeployAttack import Bettercap
from wseka_settings.models import Wseka_config
from .models import Requests
from django.utils.translation import ugettext_lazy as _


# Create your views here.
class DataPageView(TemplateView):
    template_name = "wseka_data/home.html"


def collect_data(request):
    wseka_config = get_object_or_404(Wseka_config, inuse=True)
    bettercap_api_config = {'user': wseka_config.bettercap_user, 'password': wseka_config.bettercap_password, 'host': wseka_config.bettercap_host, 'port': wseka_config.bettercap_port}
    bettercap = Bettercap(bettercap_api_config)
    events = bettercap.get_events()
    for event in events:
        if event["tag"] == "net.sniff.dns":
            #r, created = Requests.objects.get_or_create(domain_name=event["data"]["message"].split("\x1b[33m")[1].split("\x1b[0m")[0], protocol=event["data"]["protocol"])
            r, created = Requests.objects.get_or_create(domain_name=event["data"]["message"].split(": ")[1].split(" ")[0], protocol=event["data"]["protocol"])
            r.increase()
        elif event["tag"] == "net.sniff.https":
            r, created = Requests.objects.get_or_create(domain_name=event["data"]["to"], protocol=event["data"]["protocol"])
            r.increase()
        elif event["tag"] == "net.sniff.http.request":
            r, created = Requests.objects.get_or_create(domain_name=event["data"]["to"], protocol="http")
            r.increase()
        else:
            print(event)
    return HttpResponse(_("Data collected successfully"))


def export_data(request):
    data = []
    try:
        if "protocol" in request.GET.dict():
            #requests = get_list_or_404(Requests, protocol=request.GET.get("protocol"))
            requests = Requests.objects.all().filter(protocol=request.GET.get("protocol"))
        else:
            #requests = get_list_or_404(Requests)
            requests = Requests.objects.all()
        for request in requests:
            data.append({"domain_name": request.domain_name, "requested_times": request.requested_times, "protocol": request.protocol})
        return HttpResponse(str(data).replace("'", '"'))
    except Requests.DoesNotExist:
        return HttpResponse(str(data).replace("'", '"'))
