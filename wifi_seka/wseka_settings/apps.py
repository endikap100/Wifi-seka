from django.apps import AppConfig


class WsekaSettingsConfig(AppConfig):
    name = 'wseka_settings'
