
from django.forms import CharField, Form, ModelForm, Textarea
from wseka_settings.models import Wseka_config

'''
class ConfigForm(Form):
    name = CharField(label='Enter the name', max_length=100)
    ssid = CharField(label='Enter the ssid', max_length=100)
'''

class ConfigForm(ModelForm):
    class Meta:
        model = Wseka_config
        exclude = ('inuse',)
        widgets = {
            'description': Textarea(attrs={'cols': 40, 'rows': 20}),
        }

    def save(self, commit=True, *args, **kwargs):
        config = super(ConfigForm, self).save(commit=False, *args, **kwargs)
        if commit:
            config.save()
        return config