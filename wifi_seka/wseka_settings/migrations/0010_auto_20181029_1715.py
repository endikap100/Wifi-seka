# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-29 17:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wseka_settings', '0009_auto_20181029_1610'),
    ]

    operations = [
        migrations.RenameField(
            model_name='wseka_config',
            old_name='message_to_inject',
            new_name='code_to_inject',
        ),
        migrations.RenameField(
            model_name='wseka_config',
            old_name='inject_javascript',
            new_name='inject_javascript_on_http',
        ),
        migrations.AddField(
            model_name='wseka_config',
            name='inject_javascript_on_https',
            field=models.BooleanField(default=False),
        ),
    ]
