from django.db import models
from django.urls import reverse


# Create your models here.
class Wseka_config(models.Model):
    name = models.CharField(max_length=20,blank=False)
    inuse = models.BooleanField(default=False)
    ssid = models.CharField(default="Wifi-Seka",max_length=20)
    wifi_interface = models.CharField(max_length=20, default="wlan0")
    internet_interface = models.CharField(max_length=20, default="eth0")
    nat = models.BooleanField(default=True)
    mana = models.BooleanField(default=False)
    inject_javascript_on_http = models.BooleanField(default=True)
    inject_javascript_on_https = models.BooleanField(default=False)
    code_to_inject = models.TextField(default='alert("Insecure wifi access point")')
    bettercap_user = models.CharField(max_length=20, default="bettercap")
    bettercap_password = models.CharField(max_length=20, default="bettercap")
    bettercap_host = models.GenericIPAddressField(default="127.0.0.1")
    bettercap_port = models.IntegerField(default=8011)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('settings', kwargs={'pk': self.pk})
