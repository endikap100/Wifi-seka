from django.test import TestCase

# Create your tests here.
import datetime
from django.urls import reverse
from .models import Wseka_config
from django.contrib.auth.models import User


class Wseka_config_DetailViewTests(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()

    def test_redirect_in_detail_if_not_logged_in(self):
        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('detail_configuration', args=(wseka_config.pk,)))
        self.assertRedirects(response, '/account/login/?next='+reverse('detail_configuration', args=(wseka_config.pk,)))

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('detail_configuration', args=(wseka_config.pk,)))

        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')


        response = self.client.get(reverse('detail_configuration', args=(1,)))

        self.assertEqual(response.status_code, 404)


class Wseka_config_ViewTests(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()

    def test_redirect_in_detail_if_not_logged_in(self):
        response = self.client.get(reverse('settings'))
        self.assertRedirects(response, '/account/login/?next='+reverse('settings'))

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        response = self.client.get(reverse('settings'))

        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)

    def test_no_data_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        response = self.client.get(reverse('settings'))

        text="No configurations are available, create new one."

        self.assertContains(response,text)
        self.assertEqual(response.status_code, 200)

    def test_with_data_template(self):
        login = self.client.login(username='admin', password='adminadmin')
        wseka_config = Wseka_config.objects.create(name="test_config_wdqdwq")

        response = self.client.get(reverse('settings'))

        text="test_config_wdqdwq"

        self.assertContains(response,text)
        self.assertEqual(response.status_code, 200)


class Wseka_config_CreateViewTests(TestCase):

    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()

    def test_redirect_in_detail_if_not_logged_in(self):

        response = self.client.get(reverse('add_configuration'))
        self.assertRedirects(response, '/account/login/?next='+reverse('add_configuration'))

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        response = self.client.get(reverse('add_configuration'))

        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)

    def test_new_config(self):
        login = self.client.login(username='admin', password='adminadmin')
        response = self.client.post(reverse('add_configuration'), {'name': 'new_conf_wonfwd',
                                                                   'inuse': False,
                                                                   'ssid':"proba",
                                                                   'wifi_interface':'i1',
                                                                   'internet_interface': 'i2',
                                                                   'nat': True,
                                                                   'mana': False,
                                                                   'inject_javascript_on_http': False,
                                                                   'inject_javascript_on_https': False,
                                                                   'code_to_inject': 'alert("proba")',
                                                                   'bettercap_user': 'bett',
                                                                   'bettercap_password': 'bett',
                                                                   'bettercap_host': "127.0.0.1",
                                                                   'bettercap_port': 8002,
                                                                   })

        self.assertRedirects(response, reverse('settings'))
        self.assertIsNotNone(Wseka_config.objects.get(name='new_conf_wonfwd'))


class Wseka_config_UpdateViewTests(TestCase):

    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()


    def test_redirect_in_detail_if_not_logged_in(self):
        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('update_configuration', args=(wseka_config.pk,)))
        self.assertRedirects(response, '/account/login/?next='+reverse('update_configuration', args=(wseka_config.pk,)))

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('update_configuration', args=(wseka_config.pk,)))

        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)

    def test_wseka_config_update(self):
        wseka_config = Wseka_config.objects.create(name="test_config_wdqdwq")
        login = self.client.login(username='admin', password='adminadmin')
        response = self.client.post(reverse('update_configuration',
                                            args=(wseka_config.pk,)), {'name': 'new_conf_edited',
                                                                       'inuse': False,
                                                                       'ssid': "proba",
                                                                       'wifi_interface': 'i1',
                                                                       'internet_interface': 'i2',
                                                                       'nat': True,
                                                                       'mana': False,
                                                                       'inject_javascript_on_http': False,
                                                                       'inject_javascript_on_https': False,
                                                                       'code_to_inject': 'alert("proba")',
                                                                       'bettercap_user': 'bett',
                                                                       'bettercap_password': 'bett',
                                                                       'bettercap_host': "127.0.0.1",
                                                                       'bettercap_port': 8002,
                                                                       })

        self.assertRedirects(response, reverse('detail_configuration', args=(wseka_config.pk,)))
        wseka_config_edited = Wseka_config.objects.create(name='new_conf_edited')
        self.assertEqual(wseka_config_edited.name, 'new_conf_edited')


class Wseka_config_DeleteViewTests(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='admin', password='adminadmin')
        test_user.save()


    def test_redirect_in_detail_if_not_logged_in(self):
        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('detail_configuration', args=(wseka_config.pk,)))
        self.assertRedirects(response, '/account/login/?next=/settings/details/'+str(wseka_config.pk)+'/')

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='admin', password='adminadmin')

        wseka_config = Wseka_config.objects.create(name="test")
        response = self.client.get(reverse('detail_configuration', args=(wseka_config.pk,)))

        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
