from django.conf.urls import url
from wseka_settings.views import SettingsView, AddConfig, UpdateConfig, DeleteConfig, ConfigDetail, deploy, stop_attack

urlpatterns = [
    url(r"^$", SettingsView.as_view(), name="settings"),
    url(r'^new/$', view=AddConfig.as_view(), name='add_configuration'),
    url(r'^update/(?P<pk>\d+)/$', view=UpdateConfig.as_view(), name='update_configuration'),
    url(r'^delete/$', view=DeleteConfig.as_view(), name='delete_configuration'),
    url(r'^delete/(?P<pk>\d+)/$', view=DeleteConfig.as_view(), name='delete_configuration'),
    url(r'^details/(?P<pk>\d+)/$', view=ConfigDetail.as_view(), name='detail_configuration'),
    url(r'^deploy/(?P<pk>\d+)/$', view=deploy, name='deploy_configuration'),
    url(r'^stop/$', view=stop_attack, name='stop_configuration')
]